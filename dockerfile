FROM node:8.9-alpine

MAINTAINER amazingboy

ADD . /app/

WORKDIR /app

RUN npm install --production

EXPOSE 3003
CMD ["npm", "start"]